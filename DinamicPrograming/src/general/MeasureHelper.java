package backpackProblem;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Random;

public class MeasureHelper {

	public static void main(String[] args) 
	{
		Random rand = new Random();
		long[] times = new long[2000];
		System.out.println("Thief 1 ");
		int counter = 0;
		for(int i= 10; i<20000; i+=10)
		{
			System.out.println(i);
			int[] values = new int[i];
			int[] weight = new int[i];
			int generatorCounter = 0;
			while(generatorCounter < i)
			{
				values[generatorCounter] =rand.nextInt(89)+10;	
				weight[generatorCounter] = rand.nextInt(89)+10;
				generatorCounter++;
				
			}
	        //k = 25*n
			int maxKilos = 25 * i;
			Thiefs thief = new Thiefs(weight, values, maxKilos);	
			
			long start = System.currentTimeMillis();
			thief.thief3Idea();
			long end = System.currentTimeMillis();
			
			times[counter] = end-start;
			counter++;
			
		}
		
		
		storeInFile("Thief_3.txt", times);
		
	}
	
	
	
	
	public static void storeInFile(String filename, long[] times)
	{
		try 
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
			int counter = 10;
			for(int i = 0; i< times.length; i++)
			{
				writer.write("Time for loop of size "+counter +": "+ times[i]+"\n");
				counter+= 10;
			}
			writer.close();
		}catch(Exception e)
		{
			System.out.println("There was an error writing in a file");
		}
	}

}
