package general;

public class Jewel 
{
	
	private double value;
	private double weight;
	private int name;
	private boolean isStolen;
	private double percentage;
	private double stats;
	
	public double getStats() {
		return stats;
	}

	public void setStats(double stats) {
		this.stats = stats;
	}

	public Jewel(int index, int value, int weight)
	{
		this.value = value;
		this.name = index;
		this.weight = weight;
		this.stats = this.value/this.weight;
		isStolen = false;
		percentage = 100;
	}
	
	public double getPercentage() {
		return percentage;
	}

	public void setPercentage(double percentage2) {
		this.percentage = percentage2;
	}

	public boolean isStolen() {
		return isStolen;
	}

	public void setStolen(boolean isStolen) {
		this.isStolen = isStolen;
	}

	public double getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public int getName() {
		return name;
	}
	public void setName(int name) {
		this.name = name;
	}
	
	
	

}
