package general;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import knapsackProblemDinamic.Thiefs;

public class FileHelper
{

	static public Thiefs read(String filename)
	{
		ArrayList<Jewel> myList = new ArrayList<>();
		double backpackWeight =-1;
		try
		{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		int line = 0;
		
		int numberOfElement = -1;
		int index = 0;
		while(reader.ready())
		{
			
			switch(line)
			{
			case 0: 
				numberOfElement = Integer.valueOf(reader.readLine());
				line++;
				break;	
			case 1:
				backpackWeight = Integer.valueOf(reader.readLine());;
				line++;
				break;
			default:
				String currentLine = reader.readLine();
				String[] splitedLine = currentLine.split(" ");
				
				myList.add(new Jewel(index, Integer.valueOf(splitedLine[1]),Integer.valueOf(splitedLine[0])));
				index++;
			}
			
		}
		
	}catch (Exception e)
		{
		System.out.println("There was an error reading the file");
		}
		return new Thiefs(myList,backpackWeight);
	}
	
	
}
	
