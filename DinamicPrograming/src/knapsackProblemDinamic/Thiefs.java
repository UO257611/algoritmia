package knapsackProblemDinamic;

import java.util.ArrayList;

import general.FileHelper;
import general.Jewel;
import general.valueCompartor;

public class Thiefs 
{
	
	private double maxWeight = 0;
	private double[][] solution;
	private ArrayList<Jewel> theJewels ;
	private ArrayList<Jewel> stolen = new ArrayList<>();
	
	public static void  main (String[] args)
	{
		Thiefs thiefs = FileHelper.read("botin04.txt");
		thiefs.knapsackProble();
		thiefs.printMatrix();
		thiefs.printStolen();
		
	}
	
	public Thiefs(ArrayList<Jewel> jewels, double maxWeight)
	{
	
		theJewels = jewels;
		this.maxWeight = maxWeight;
		solution = new double[theJewels.size()+1][(int)maxWeight+1];
		theJewels.sort(new valueCompartor());
	}
	
	
	
	public void knapsackProble()
	{
		for(int i = 0; i <= theJewels.size(); i++)
		{
			for (int w = 0; w<=maxWeight; w++)
			{
				if(w == 0 || i == 0)
				{
					solution[i][w] = 0;
				}
				else if ( theJewels.get(i-1).getWeight() <= w)
				{
					solution[i][w] = max(solution[i-1][(int) (w - theJewels.get(i-1).getWeight())]+ 
							theJewels.get(i-1).getValue(),  theJewels.get(i-1).getValue()) ; 
					
				}
				else
				{
					solution[i][w] = solution[i-1][w];
				}
				
				
			}
		}
		
		//w - theJewels.get(i-1).getWeight()
		int x = (int) maxWeight;
		int index = theJewels.size();

		while(x >0)
		{
			if(solution[index][x] == solution[index-1][x])
			{
				index--;
			}
			else if(solution[index][x-1] !=solution[index][x] )
			{			stolen.add(theJewels.get(index-1));
			x -= theJewels.get(index-1).getWeight();
			index--;
			}
			else
			{
				x--;
			}

		}
		
		
		
		
	}
	
	
	public double max (double a, double b)
	{
		return(a>b)? a:b;
	}
	
	
	public void printMatrix()
	{
		for (int i = 0; i< solution.length; i++)
		{
			for (int j = 0; j<solution[i].length; j++)
			{
				System.out.print( solution[i][j] +"\t");
			}
			System.out.println();
		}
	}
	
	public void printStolen()
	{
		StringBuffer x = new StringBuffer("r =[ ");
		for(Jewel missing : stolen)
		{
			x.append(missing.getWeight());
			x.append(", ");
		}
		
		x.append("]");
		System.out.println(x.toString());
	}
	

}
