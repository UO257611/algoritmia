package labs.lab1_2;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Random;

//This class needs to loop1, loop2 and loop 3 in order to work fine.
public class MeasurementHelper 
{
	public static  void main(String arg[])
	{
		MeasurementHelper helper = new MeasurementHelper();
		/*System.out.println("Loop 1");
		long[] loop1 =   helper.getLoop1Data();
		
		System.out.println("Loop 2");
		long[] loop2 =   helper.getLoop2Data();
		
		helper.StoreData(loop1, loop2, "Loop 4-5");
		
		long[] loop4 =   helper.getLoop4Data();
		
		System.out.println("Loop 5");
		long[] loop5 =   helper.getLoop5Data();
		
		helper.StoreData(loop4, loop5, "Loop 4-5");
		
		*/
		
		System.out.println("Loop 2");
		long[] loop2 =   helper.getLoop2Data();
		
		System.out.println("Loop 3");
		long[] loop3 =   helper.getLoop3Data();
		
		helper.StoreData(loop2, loop3, "Loop 2-3");
	}
	
	
	
	/**
	 * This only a warmup method to set up the JVM
	 */
	public void warmUp(int selector)
	{
		switch(selector) {
		case 1: 
			for (int i = 0 ; i<200; i++)
			{
				Loop1.loop1(50);
				
			}
			break;
		
		case 2:
			for (int i = 0 ; i<200; i++)
			{
				Loop2.loop2(50);
				
			}
			break;
		case 3:
			for (int i = 0 ; i<200; i++)
			{
				Loop3.loop3(50);
				
			}
			break;
		
		case 4:
			for (int i = 0 ; i<200; i++)
			{
				loop4(50);
				
			}
			break;
		
		case 5:
			for (int i = 0 ; i<200; i++)
			{
				loop5(50);
				
			}
			break;
		}
		
	}
	
	/**
	 * This method is for obtain the data in reltaion with loop 1
	 * @return times for loop1
	 */
	public long[] getLoop1Data()
	{
		int counter = 0;
		long[] theData = new long[17]; 
		warmUp(1);
		for (int n = 8; n <= 524288; n *= 2 )
		{
			System.out.println(n);
			long start = System.currentTimeMillis();
			Loop1.loop1(n);
			long end = System.currentTimeMillis();
			theData[counter] = end - start;
			counter++;
		}
		return theData;
		
	}
	
	

	/**
	 * This method is for obtain the data in reltaion with loop 2
	 * @return times for loop2
	 */
	public long[] getLoop2Data()
	{
		int counter = 0;
		long[] theData = new long[17]; 
		warmUp(2);
		for (int n = 8; n<= 131072; n *= 2 )
		{

			System.out.println(n);
			
			
			long start = System.currentTimeMillis();
			Loop2.loop2(n);
			long end = System.currentTimeMillis();
			theData[counter] = end - start;
			counter++;
		}
		return theData;
		
	}
	
	

	


	/**
	 * This method is for obtain the data in reltaion with loop 4
	 * @return times for loop4
	 */
	public long[] getLoop4Data()
	{
		int counter = 0;
		long[] theData = new long[17]; 
		warmUp(5);
		for (int n = 8; n <= 524288; n *= 2 )
		{

			System.out.println(n);
			
			
			long start = System.currentTimeMillis();
			loop4(n);
			long end = System.currentTimeMillis();
			theData[counter] = end - start;
			counter++;
		}
		return theData;
		
	}
	

	/**
	 * This method is for obtain the data in reltaion with loop 3
	 * @return times for loop3
	 */
	public long[] getLoop3Data()
	{
		int counter = 0;
		long[] theData = new long[17]; 
		warmUp(3);
		for (int n = 8; n<= 131072; n *= 2 )
		{

			System.out.println(n);
			
			
			long start = System.currentTimeMillis();
			Loop3.loop3(n);
			long end = System.currentTimeMillis();
			theData[counter] = end - start;
			counter++;
		}
		return theData;
		
	
	}
	

	/**
	 * This method is a version of the loop methods wit O(n^4)
	 */
	public void loop4(int n)
	{
		Random rn = new Random();
		int cont = 0;
		for (int i=1; i<=n; i++)
			for (int j=1; j<=n; j++) 
				for (int k =1; k<=n; k++)
					for (int  z=1; z<=n; z++)
						cont += rn.nextInt();
		
		
	}

	
	/**
	 * The aim of this method is obtain the time for the loop 5
	 */
	public long[] getLoop5Data()
	{
		int counter = 0;
		long[] theData = new long[17]; 
		warmUp(5);
		for (int n = 8; n<= 4096; n *= 2 )
		{

			System.out.println(n);
			
			
			long start = System.currentTimeMillis();
			loop5(n);
			long end = System.currentTimeMillis();
			theData[counter] = end - start;
			counter++;
		}
		return theData;
		
	}
	
	
	


	/**
	 * This method is a version of the loop methods wit O(n^3 log n)
	 */
	
	public void loop5(int n)
	{
		Random rn = new Random();
		int cont = 0;
		for (int i=1; i<=n; i++)
			for (int j=1; j<=n; j++) 
				for (int k =1; k<=n; k++)
					for (int  z=1; z<=n; z*=2)
						cont += rn.nextInt();
		
		
	}
	
/**
 * This method only writes in a file the time of two versions of the loop
 */
	public void StoreData(long[] data1, long[] data2, String filename)
	{
		try 
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
			writer.write("Loop 1, 2 or 4 \n \n");
			for(long value : data1)
			{
				writer.write(value + "\n");
			}
		
			writer.write("\n \n");
			
			writer.write("Loop 2, 3 or 5 \n \n");
			for(long value : data2)
			{
				writer.write(value + "\n");
			}
			
			writer.close();
		}catch(Exception e)
		{
			System.out.println(e.getStackTrace());
		}
	}
	
}
