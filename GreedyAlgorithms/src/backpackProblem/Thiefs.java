package backpackProblem;

import java.util.ArrayList;

import javax.rmi.CORBA.Util;

public class Thiefs {

	public ArrayList<Jewel> jewels = new ArrayList<>();
	private ArrayList<Jewel> backpack = new ArrayList<>(); 
	private double maxWeight;
	private double backpackWeight;
	
	public Thiefs( int[] weight, int[] values, int maxWeight)
	{
		backpackWeight= 0;
		this.maxWeight = maxWeight;
		for(int i = 0; i< values.length; i++)
		{
			jewels.add(new Jewel(i+1, values[i], weight[i]));
		
		}
		
	}
	
	public static void main(String[] args) 
	{
		int[] weight = {10,2,3,40,3,6,2,6, 7};
		int[] values = {1,100,2,30,5,3,5,10,9};
	
		
		Thiefs thiefs = new Thiefs( weight, values, 30);
		
		thiefs.thief3Idea();
		System.out.println("Backpack weight " +thiefs.backpackWeight);
		
		
		

	}

	

	
	public void thief1Idea()
	{
		
		while (maxWeight != backpackWeight)
		{
			Jewel tempJewel = null;
			for(int i = 0; i< jewels.size(); i++)
			{
				if(tempJewel == null && !jewels.get(i).isStolen())
				{
					tempJewel = jewels.get(i);
				}
				else if(!jewels.get(i).isStolen() && jewels.get(i).getWeight()<tempJewel.getWeight())
					tempJewel = jewels.get(i);
			}
			
			if(backpackWeight + tempJewel.getWeight() <= maxWeight)
			{
				tempJewel.setStolen(true);
				backpack.add(tempJewel);
				backpackWeight += tempJewel.getWeight();
			//	printInformation(tempJewel);
			}
			else if(maxWeight-backpackWeight > 0)
			{
				double percentage = ( (maxWeight-backpackWeight)/ tempJewel.getWeight() )* 100;
				tempJewel.setStolen(true);
				tempJewel.setPercentage(percentage);
				backpack.add(tempJewel);
				backpackWeight = maxWeight;
			//	printInformation(tempJewel);
				break;
			
			}
			else
			{
				break;
			}
			
			
		}
		
	}
	
	public void thief2Idea()
	{
		
		orderIt();
		
		
		while (maxWeight != backpackWeight)
		{
			Jewel tempJewel = null;
			for(int i = 0; i< jewels.size(); i++)
			{
				if(tempJewel == null && !jewels.get(i).isStolen())
				{
					tempJewel = jewels.get(i);
				}
				else if(!jewels.get(i).isStolen() && jewels.get(i).getValue()>tempJewel.getValue())
					tempJewel = jewels.get(i);
			}
			
			if(backpackWeight + tempJewel.getWeight() <= maxWeight)
			{
				tempJewel.setStolen(true);
				backpack.add(tempJewel);
				backpackWeight += tempJewel.getWeight();
			//	printInformation(tempJewel);
			}
			else if(maxWeight-backpackWeight > 0)
			{
				double percentage = ( (maxWeight-backpackWeight)/ tempJewel.getWeight() )* 100;
				tempJewel.setStolen(true);
				tempJewel.setPercentage(percentage);
				backpack.add(tempJewel);
				backpackWeight = maxWeight;
			//	printInformation(tempJewel);
				break;
			
			}
			else
			{
				break;
			}
			
			
		}
		
	}
	
	
	private void orderIt() 
	{
		
			
			double tempValue = -1;
			for(int i = 0; i< jewels.size(); i++) 
			{
				int posMin=i;
				for(int j = i; j< jewels.size(); j++)
				{
					if(jewels.get(j).getValue() > tempValue)
					{
						posMin = j;
						tempValue = jewels.get(j).getValue();
					}
				}
				Jewel temp = jewels.get(i);
				jewels.set(i, jewels.get(posMin));
				jewels.set(posMin, temp);
			}
		
		
	}

	public void thief3Idea()
	{
		jewels.sort(new valueCompartor());
		while (maxWeight != backpackWeight)
		{
			Jewel tempJewel = null;
			for(int i = 0; i< jewels.size(); i++)
			{
				if(tempJewel == null && !jewels.get(i).isStolen())
				{
					tempJewel = jewels.get(i);
				}
				else if(!jewels.get(i).isStolen() && jewels.get(i).getStats()>tempJewel.getStats())
					tempJewel = jewels.get(i);
			}
			
			if(backpackWeight + tempJewel.getWeight() <= maxWeight)
			{
				tempJewel.setStolen(true);
				backpack.add(tempJewel);
				backpackWeight += tempJewel.getWeight();
			//	printInformation(tempJewel);
			}
			else if(maxWeight-backpackWeight > 0)
			{
				double percentage = ( (maxWeight-backpackWeight)/ tempJewel.getWeight() )* 100;
				tempJewel.setStolen(true);
				tempJewel.setPercentage(percentage);
				backpack.add(tempJewel);
				backpackWeight = maxWeight;
			//	printInformation(tempJewel);
				break;
			
			}
			else
			{
				break;
			}
			
			
		}
		
	}
	
		
	
	
	
	

	private void printInformation(Jewel tempJewel) {
		System.out.println("The thief took the jewel "+tempJewel.getPercentage()+"% of the jewel with index"
				+ " "+ tempJewel.getName());
		System.out.println("This jewel weight "+ tempJewel.getWeight() + " and has a value of " +tempJewel.getValue());
	}

	
	
	
	
	

}
