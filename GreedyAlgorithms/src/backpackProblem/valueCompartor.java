package backpackProblem;

import java.util.Comparator;

public class valueCompartor implements Comparator<Jewel>
{

	@Override
	public int compare(Jewel o1, Jewel o2) 
	{
		if(o1.getStats() >= o2.getStats())
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}


}
