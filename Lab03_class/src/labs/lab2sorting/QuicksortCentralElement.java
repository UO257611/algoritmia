package labs.lab2sorting;

/* This program can be used to sort n elements with 
 * the best algorithm. It is the QUICKSORT */
public class QuicksortCentralElement
	{
	static int []v;
	
	public static void main (String arg [] ){
	  int n=  1000;//Integer.parseInt(arg[0]);  //size of the problem
	  v = new int [n];
	
	  Vector.sorted(v);
	  System.out.println("VECTOR TO BE SORTED:");
	  Vector.write(v);	
	  quicksort(v);
	  System.out.println("SORTED VECTOR:");
	  Vector.write(v);
	
	  Vector.inverselySorted (v);
	  System.out.println("VECTOR TO BE SORTED:");
	  Vector.write(v);	
	  quicksort(v);
	  System.out.println("SORTED VECTOR:");
	  Vector.write(v);
	  
	  Vector.random(v, 1000000);
	  System.out.println("VECTOR TO BE SORTED:");
	  Vector.write(v);	
	  quicksort(v);
	  System.out.println("SORTED VECTOR:");
	  Vector.write(v);
	} 
	
	private static void quickSort(int elements[], int left, int right){
		int i = left;
		int j = right;
		
		if(left< right) {
			if(elements.length > 2)
			{
				int center= (left + right )/2;
				int pivot = elements[center];
				Util.interchange(elements, center, right); 
				
				do
				{
					while (elements[i] <= pivot && i<right) {i++;}
					while (elements[j] >= pivot && j>left ) {j--;}
					if( i< j ) { Util.interchange(elements, i, j);} 
				}while(i<j);
				
				Util.interchange(elements, i, right);
				quickSort(elements, left, i -1);
				quickSort(elements, i+1, right);
			}
			else
			{
				if(elements[i] > elements[j])
				{
					Util.interchange(elements, i, j);
				}
			}
			}
		}
		
		
		
		
	
	public static void quicksort(int[] elements) {
		quickSort(elements, 0, elements.length-1);
	}
} 
