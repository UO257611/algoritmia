package labs.lab2sorting;

/* This program can be used to sort n elements with 
 * a "bad" algorithm (quadratic). 
 * It is the DIRECT INSERTION */
public class Insertion{
	static int []v;
	
	public static void main (String arg [] ){
	  int n= 35;//Integer.parseInt (arg[0]);  //size of the problem 
	  v = new int [n] ;
	  
	  Vector.sorted(v);
	  System.out.println ("VECTOR TO BE SORTED:");
	  Vector.write(v);	
	  insertion(v);
	  System.out.println ("SORTED VECTOR:");
	  Vector.write(v);
	
	  Vector.inverselySorted (v);
	  System.out.println ("VECTOR TO BE SORTED:");
	  Vector.write(v);	
	  insertion(v);
	  System.out.println ("SORTED VECTOR:");
	  Vector.write(v);
	
	  Vector.random(v, 1000000);
	  System.out.println ("VECTOR TO BE SORTED:");
	  Vector.write(v);	
	  insertion(v);
	  System.out.println ("SORTED VECTOR:");
	  Vector.write(v);
	} 
	
	public static void insertion(int[] elements)
	{
		int j= -1111;
		int pivot= -1111;
		
		for (int i = 1; i<elements.length; i++)
		{
			pivot = elements[i];
			j = i-1;
		
		while ( j>= 0 && pivot < elements[j])
		{
			elements[j+1] = elements[j];
			j--;
		}
		elements[j+1] = pivot;
		}
	}
	
	
} 
