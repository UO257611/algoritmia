package labs.lab2sorting;

/* This program can be used to sort n elements with 
 * a "bad" algorithm (quadratic). 
 * It is the BUBBLE or DIRECT EXCHANGE */
public class Bubble{
	static int []v;

	public static void main (String arg [] ){
	  int n=  1000000; ///Integer.parseInt(arg[0]);//  size of the problem
	  v = new int[n] ;
	
	  Vector.sorted(v);
	  System.out.println("VECTOR TO BE SORTED:");
	  Vector.write(v);	
	  bubble(v);
	  System.out.println("SORTED VECTOR:");
	  Vector.write (v);
	
	  Vector.inverselySorted(v);
	  System.out.println("VECTOR TO BE SORTED:");
	  Vector.write(v);	
	  bubble(v);
	  System.out.println("SORTED VECTOR:");
	  Vector.write(v);
	
	  Vector.random(v, 1000000);
	  System.out.println("VECTOR TO BE SORTED:");
	  Vector.write(v);	
	  bubble(v);
	  System.out.println("SORTED VECTOR:");
	  Vector.write(v); 
	} 
	/*Theory slides
	public static void bubble(int[] elements) 
	{
		for (int i = 1 ;i < elements.length; i++)
		{
			for (int j = elements.length - 1; j>= i; j--)
			{
				if(elements[j-1]> elements[j])
				{
					Util.interchange(elements, j-1, j);
				}
			}
		}
	}*/
	
	//Bubble upgrade "Bubble with sentinel"
	public static void bubble(int[] elements) 
	{
		boolean hasChanged = true;
		int counter = 0;
		while(hasChanged && counter < elements.length)
		{
			hasChanged= false;
			for (int i = elements.length-1 ; i>counter; i--)
			{
				if( elements[i-1] > elements[i])
				{
					Util.interchange(elements, i-1, i);
					hasChanged= true;
				}
				
			}
			counter++;
			
			
		}
		
		
	}
	
} 

