package labs.lab2sorting;

import java.io.BufferedWriter;
import java.io.Console;
import java.io.FileWriter;

public class DataHelper 
{

	
	public static void main(String[] arg)
	{
		DataHelper helper = new DataHelper();
		
		helper.warmUp();
		System.out.println("Bubble start");
		long[] sorted = helper.getTimeBubble('s');
		//long[] invert = helper.getTimeBubble('i');
		//long[] random = helper.getTimeBubble('r');
		
		helper.writeInFile(sorted, sorted, sorted, "Bubble_Data");
		

		System.out.println("Bubble done");
		 /*
		System.out.println("Insertion start");
		
		long[] sorted = helper.getTimeInserton('s');
		long[] invert = helper.getTimeInserton('i');
		long[] random = helper.getTimeInserton('r');

		helper.writeInFile(sorted, invert, random, "Insertion_Data");

		System.out.println("Insertion end");
		
		System.out.println("Selection start");
		long[] sorted = helper.getTimeSelection('s');
		long[] invert = helper.getTimeSelection('i');
		long[] random = helper.getTimeSelection('r');

		helper.writeInFile(sorted, invert, random, "Selection_Data");

		System.out.println("Selection end");
		
		System.out.println("Quick start");
		long[] sorted = helper.getTimeQuicksort('s');
		long[] invert = helper.getTimeQuicksort('i');
		long[] random = helper.getTimeQuicksort('r');

		helper.writeInFile(sorted, invert, random, "Quicksort_Data");

		System.out.println("Quick end");*/
	}
	
	private void warmUp()
	{
		int[] x = new int[1000];
		Vector.random(x,100);
		for (int i= 0; i< 100; i++)
		{
			Bubble.bubble(x);
		}
		
		Vector.random(x,100);
		
		for (int i= 0; i< 100; i++)
		{
			Insertion.insertion(x);
		}
		
		Vector.random(x,100);
		
		for (int i= 0; i< 100; i++)
		{
			Selection.selection(x);
		}
		
		Vector.random(x,100);
		
		for (int i= 0; i< 100; i++)
		{
			QuicksortCentralElement.quicksort(x);
		}
	}
	
	
	private long[] getTimeBubble(char selector)
	{
		long start = -1,end=-1;
		long[] times = new long[18];
		int counter= 0;
		for (int n= 5000; n<= 160000000; n*=2)
		{
		System.out.println(n);
	    int[] vector = new int[n];
	    
	    prepareVecor(vector, selector);
	    
		start = System.currentTimeMillis();
		Bubble.bubble(vector);
		end = System.currentTimeMillis();
		
		times[counter] = end-start;
		counter++;
		
		}
		
		return times;
	}
	
	
	
	
	private long[] getTimeInserton(char selector)
	{
		long start = -1,end=-1;
		long[] times = new long[18];
		int counter= 0;
		for (int n= 5000; n<= 1280000*2; n*=2)
		{
			System.out.println(n);
			
	    int[] vector = new int[n];
	    
	    prepareVecor(vector, selector);
	    
		start = System.currentTimeMillis();
		Insertion.insertion(vector);
		end = System.currentTimeMillis();
		
		times[counter] = end-start;
		
		counter++;
		
		}
		
		return times;
	}
	
	
	private long[] getTimeSelection(char selector)
	{
		long start = -1,end=-1;
		long[] times = new long[18];
		int counter= 0;
		for (int n= 5000; n<= 1280000; n*=2)
		{

			System.out.println(n);
			
	    int[] vector = new int[n];
	    
	    prepareVecor(vector, selector);
	    
		start = System.currentTimeMillis();
		Selection.selection(vector);
		end = System.currentTimeMillis();
		
		times[counter] = end-start;
		
		counter++;
		
		}
		
		return times;
	}
	
	
	private long[] getTimeQuicksort(char selector)
	{
		long start = -1,end=-1;
		long[] times = new long[18];
		int counter= 0;
		for (int n= 5000; n<= 81920000; n*=2)
		{

			System.out.println(n);
	    int[] vector = new int[n];
	    
	    prepareVecor(vector, selector);
	    
		start = System.currentTimeMillis();
		QuicksortCentralElement.quicksort(vector);
		end = System.currentTimeMillis();
		
		times[counter] = end-start;
		
		counter++;
		
		}
		
		return times;
	}
	
	
	
	private void prepareVecor(int[] vector,char selector)
	{
		
		
		switch(selector)
		{
			case 's':
				for(int i = 0; i<vector.length; i++)
					vector[i] = i;
				break;
				
			case 'i':
				Vector.inverselySorted(vector);
				break;
				
			case 'r':
				Vector.random(vector, 10000);
				
				
		}
		
	}
	
	private void writeInFile(long[] timeSorted, long[] timeInvers, long[] timeRandom, String nameFile )
	{
		try
		{
		BufferedWriter writer = new BufferedWriter(new FileWriter(nameFile));
		
		writer.write(nameFile + "\n\n");
		writer.write("Time in sorted \n");
		for(long i : timeSorted)
		{
			writer.write(i +"\n");
		}
		
		writer.write("Time in inverted \n");
		for(long i : timeInvers)
		{
			writer.write(i +"\n");
		}
		writer.write("Time in random \n");
		for(long i : timeRandom)
		{
			writer.write(i +"\n");
		}
		
		
		writer.close();
		}
		catch(Exception e)
		{
			System.out.println("Something went wrong in the process os storing the data");
		}
		
	}
	
}
	
	
	
	
	

