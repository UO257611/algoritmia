package Solution;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.Writer;

public class FileHelper {
	
	
	
	public static int[][] read(String filename)
	{
		
		int[][] data; 
		int counter = 0;
		try {
		BufferedReader reader =  new BufferedReader(new FileReader(filename));
		int n = Integer.valueOf(reader.readLine());
		data= new int[n][n];
		while(reader.ready())
		{
			String line = reader.readLine();
			String[] splittedLine = line.split("\t");
			
			for(int i = 0; i< n; i++)
			{
				data[counter][i] = Integer.valueOf(splittedLine[i]);
			}
			
			counter++;

		}
		reader.close();
		return data;
		}catch(Exception e)
		{
			System.out.println("Error in the reading from the file or creating the delivery");
			
		}
		return null;
	}
	
	
	public static void write(long[] time, String filename)
	{
		try 
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
			for(int i=0; i<time.length; i++)
			{
				writer.append(i+" ms for size "+(i+3)+" columnes; "+ time[i]+"\n");
			}
			
			writer.close();
		}
		catch(Exception e)
		{
			System.out.println("Error while writting into the file");
		}
		
	}




}
