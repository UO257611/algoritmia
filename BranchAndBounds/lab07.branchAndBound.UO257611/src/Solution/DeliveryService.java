package Solution;

import java.util.ArrayList;
import java.util.List;

import Material.BranchAndBound;
import Material.Node;
public class DeliveryService extends BranchAndBound
{
	
	protected int[][] data;
	protected static String FILE_NAME = "Data.txt";
	protected ArrayList<Integer> assigned;
	protected int bestCost = Integer.MAX_VALUE;
	protected ArrayList<Node> solution;
	private int counter ;
	
	public static void main(String[] arg)
	{
		
		DeliveryService deliver;
		deliver = new DeliveryService(FileHelper.read(FILE_NAME));
		deliver.organizeTheDelivers("ba");
		deliver.printSolutionTrace(); //prints the solution of the Branch and bound
		deliver.printBacktracking(); //prints the solution of the backtracking
		
		
	}
	
	
	public DeliveryService(int[][] data)
	{
		
		this.data = data;
		assigned = new ArrayList<Integer>();
		solution = new ArrayList<Node>();
	}
	

	
	public void organizeTheDelivers(String selector)
	{
		rootNode = new Repartidor(0, null, ds, -1, data, assigned);
		
		switch (selector) 
		{
		case "br":
			branchAndBound(rootNode); //if you want to do it by branch and boun
			
			break;

		case "ba":
			callBacktracking((Repartidor)rootNode);//if you want to do it by backtracking
			break;

		}
		
		
	}
	
	
	
	public boolean callBacktracking(Repartidor root)
	{
		ds.insert(root);
		return backtracking();
	}
	
	public boolean backtracking()
	{
		Repartidor current =( (Repartidor)ds.extractBestNode());
		if(current.isSolution() == true && bestCost> current.getHeuristicValue())
		{
			this.solution = current.heap.extractUsedNodesFrom(current);
			this.bestCost = current.getHeuristicValue();
			return true;
		}
		else
		{
			boolean found = false;
			
			if(current.getDepth() < data.length-1)
			{
			List<Node> listOfChilds = current.expand();
			for(Node child: listOfChilds)
			{
				ds.insert(child);
				if(child.getDepth()< data.length-1)
				{
					found |= backtracking();
				}
				else
				{
					found = false;
				}

			}}
			return found;
			
		}

	}
	
	
	
	public void printBacktracking()
	{
		counter = data.length-1;
		solution.forEach(x -> {System.out.println(""+counter+" repartidor tiene un coste: "+ x); counter--;});
		
	}
	
	
	
	
	
	
	
	
	
	
}
