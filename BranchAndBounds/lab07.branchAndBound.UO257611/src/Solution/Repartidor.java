package Solution;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.UUID;

import Material.Heap;
import Material.Node;

public class Repartidor extends Node
{
	protected int value;
	protected Heap heap;
	protected int[][] data;
	protected ArrayList<Integer> isUsed;
	
	
	public Repartidor(int value,  UUID parent, Heap heap, int depth, int[][] data, ArrayList<Integer> isUsed)
	{
		
		this.value = value;
		parentID = parent;
		this.heap = heap;
		this.depth = depth;
		this.data = data;
		this.isUsed = isUsed;
		calculateHeuristicValue();
		
		
	}
	
	@Override
	public void calculateHeuristicValue() 
	{
		heuristicValue = 0;
		ArrayList<Node> nodesUsed = heap.extractUsedNodesFrom(this);
		nodesUsed.forEach(x -> {heuristicValue += ((Repartidor)x).value;});
		
		
	}

	@Override
	public ArrayList<Node> expand() 
	{
		ArrayList<Node> children = new ArrayList<Node>();
		for (int j=0; j< data[depth+1].length ;j++)
		{
			if(!isUsed.contains(j))
				{
				ArrayList<Integer> updatedList = new  ArrayList<Integer>();
				isUsed.forEach( order -> updatedList.add(order));
				updatedList.add(j);
				children.add(new Repartidor(data[depth+1][j], ID, heap, depth+1, data, updatedList ));
				}
		}
		return children;
		
	}

	@Override
	public boolean isSolution() {
		// TODO Auto-generated method stub
		return depth == data.length-1;
	}
	
	
	@Override
	public String toString()
	{
		return String.valueOf(value);
	}
	
	@Override
	public int initialValuePruneLimit() {
		int tempLeft=0;
		int tempRight =0;
		for(int i =0; i< data.length; i++)
		{
			tempLeft+= data[i][i];
			tempRight+= data[data.length-1-i][data.length-1-i];
		}
		if(tempLeft<tempRight)
			return tempLeft;
		else
			return tempRight;
		
		
		
		
	}
	

}
