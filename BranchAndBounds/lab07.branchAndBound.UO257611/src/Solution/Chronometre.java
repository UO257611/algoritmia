package Solution;

import java.util.Random;

public class Chronometre {

	private static final String FILENAME_BACKTRCKING = "Backtraking_times.txt";
	private static final String FILENAME_BRANCHANDBOUND = "BranchAndBound_times.txt";
	private static final int NUMBER_OF_COLUMNS = 9;
	protected int[][] data;
	private Random random = new Random();
	private DeliveryService deliver;
	private long start ;
	private long end;
	private long[] time;
	public static void main(String[] args) {
		
		Chronometre chrono = new Chronometre();
		chrono.measureBacktracking();
		//chrono.measureBranchAndBound();
	}
	
	public void measureBacktracking()
	{
		time = new long[NUMBER_OF_COLUMNS];
		
		for(int i =3; i<NUMBER_OF_COLUMNS; i++)
		{
			generateData(i);
			deliver= new DeliveryService(data);
			
			start = System.currentTimeMillis();
			deliver.organizeTheDelivers("ba");
			end = System.currentTimeMillis();
			
			time[i] = end-start;
			System.out.println("Number of columns: " +i);
		}
		
		FileHelper.write(time, FILENAME_BACKTRCKING);
		
		
		
	}
	
	

	public void measureBranchAndBound()
	{
		time = new long[NUMBER_OF_COLUMNS];
		
		for(int i =3; i<NUMBER_OF_COLUMNS; i++)
		{
			generateData(i);
			deliver= new DeliveryService(data);
			
			start = System.currentTimeMillis();
			deliver.organizeTheDelivers("br");
			end = System.currentTimeMillis();
			
			time[i] = end-start;
			System.out.println("Number of columns: " +i);
		}
		
		FileHelper.write(time, FILENAME_BRANCHANDBOUND);
		
		
		
	}
	
	

	private void generateData(int ncolumns) 
	{
		data= new int[ncolumns][ncolumns];
		for(int i =0; i< ncolumns; i++)
			for (int j = 0; j<ncolumns; j++)
				data[i][j] = random.nextInt(100);	
	}

}
