package generator;

import java.util.List;

public interface State 
{
	public List<State> getChilds();
	public boolean isSelect();

}
