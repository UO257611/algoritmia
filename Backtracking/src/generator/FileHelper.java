package generator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileHelper 
{
	
	public static List<String> fileReader(String filename) 
	{
		ArrayList<String> pairs = new ArrayList<>();
		
		try
		{
		BufferedReader reader = new BufferedReader(new FileReader(filename));
		
		while(reader.ready())
		{
			pairs.add(reader.readLine());
		}
		}catch(IOException e)
		{
			System.out.println("Error while reading the pairs of string file");
			e.printStackTrace();
		}
		return pairs;
		
		
		
		
	}
	
	public static void fileWriter(long[] time, int vocals , int consonants ,int symbols )
	{
		
		
		try
		{
			BufferedWriter writer = new BufferedWriter(new FileWriter("Time"));
			writer.write("This file contain the time that took to generate 15 passwords: \n");
			
			
			for(int i = 0; i< time.length; i++)
				writer.write("Time for size "+(i+3)+" with "+vocals+" vocals, "+consonants+" consonants and "+ symbols+" :" + time[i]+" \n");
			
			writer.close();
		
		
		}catch(IOException e)
		{
			System.out.println("Error while reading the pairs of string file");
			e.printStackTrace();
		}
		
		
		
	}

}
