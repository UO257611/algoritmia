package generator;

import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;

public class GeneratorPassword {
	//int maxSize,int symbolsChars, char[] password, List<String> pairsOfConsonants) {
	
	List<State> solutions;
	private static String TESTFILENAME = "pairs.txt";
	private static int MAXSIZE = 6;
	private static int NUMBEROFSYMBOLS = 2;
	private static int NUMBEROFPASSWORDS =15;
	
	public static void main(String[] arg)
	{
		GeneratorPassword generator;
		///If you use the comand line to launch the program
		if(arg.length == 4)
		{
			List<String> pairs = FileHelper.fileReader(arg[3]);
			generator = new GeneratorPassword();
			MAXSIZE = Integer.valueOf(arg[0]);
			NUMBEROFSYMBOLS = Integer.valueOf(arg[1]);
			NUMBEROFPASSWORDS = Integer.valueOf(arg[2]);
			generator.allSolutionsBacktracking(new PasswordState(MAXSIZE, NUMBEROFSYMBOLS, new char[] {}, pairs));
					
		}
		else
		{
		//General test	
		List<String> pairs = FileHelper.fileReader(TESTFILENAME);
		generator = new GeneratorPassword();
		generator.allSolutionsBacktracking(new PasswordState(MAXSIZE, NUMBEROFSYMBOLS, new char[] {}, pairs));
		}
		
		for(State x: generator.solutions)
		{
			System.out.println(x.toString());
		}
		/*
		 * This was only used for measure the time
		 * 
		List<String> pairs = FileHelper.fileReader(TESTFILENAME);
		generator = new GeneratorPassword();
		long [] time = new long[20];
		MAXSIZE = 3;
		while(MAXSIZE<=9) {
		
		long start = System.currentTimeMillis();
		generator.allSolutionsBacktracking(new PasswordState(MAXSIZE, NUMBEROFSYMBOLS, new char[] {}, pairs));
		long end = System.currentTimeMillis();
		
		time[MAXSIZE-3]= end-start;
		for(State x: generator.solutions)
		{
			System.out.println(x.toString());
		}
		
		FileHelper.fileWriter(time, 3, 4, NUMBEROFSYMBOLS);
		generator.solutions.clear();
		MAXSIZE++;
		
		}
		*/
		
		
		
	
		
		
	}
	
	
	
	public  GeneratorPassword() {
		solutions = new ArrayList<State>();
	}

	public boolean firstSolutionBacktracking(State current)
	{
		if(current.isSelect() == true)
		{
			System.out.println("Hurray - first solution");
			return true;
		}
		else
		{
			List<State> listOfChilds = current.getChilds();
			boolean found = false;
			for(State child: listOfChilds)
			{
				
				found |= firstSolutionBacktracking(child);
			
				
			}
			return found;
		}
	
		
	}
	
	
	
	public boolean allSolutionsBacktracking(State current)
	{
		
		if(current.isSelect() == true && solutions.size()< NUMBEROFPASSWORDS )
		{
			
			this.solutions.add(current);
			return true;
		}
		else
		{
			List<State> listOfChilds = current.getChilds();
			boolean found = false;
			for(State child: listOfChilds)
			{
				PasswordState pass = (PasswordState)child;
				if( pass.getSize()>= 1  && pass.condition1() &&pass.condition2()  && pass.getSize()<= MAXSIZE )
					found |= allSolutionsBacktracking(child);
				

			}
			return found;
			
		}

	}
	
	
	
	
	
	
	
		
	
	
	

}
