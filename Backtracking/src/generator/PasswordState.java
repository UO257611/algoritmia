package generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PasswordState implements State 
{
	
	
	public static char[] vowels = new char[] {'a','e','i'};
	
	public static char[] consonants = new char[] { 'b','r','j','l'};
	public static char[] symbols = new char[] { '1','#','@'};
	private String vowelsString = String.valueOf(vowels);
	private String consonantsString = String.valueOf(consonants);
	private String symbolsString = String.valueOf(symbols);
	
	
	
	protected char[] password;
	protected int maxSize;
	protected int symbilcChars;
	protected List<String> pairsOfConsonants;
	
	public PasswordState(int maxSize,int symbolsChars, char[] password, List<String> pairsOfConsonants) {
		this.maxSize = maxSize;
		this.password = password;
		this.symbilcChars = symbolsChars;
		this.pairsOfConsonants = pairsOfConsonants;
	}
	
	
	
	@Override
	public List<State> getChilds() {
		
		List<State> returnList = new ArrayList<>();
		
		
		for(char c: vowels)
		{
			char[] newValue = new char[password.length+1];
			System.arraycopy(password, 0, newValue, 0, password.length);
			System.arraycopy(new char[] {c}, 0, newValue,password.length,1 );
			
			returnList.add(new PasswordState(this.maxSize, this.symbilcChars, newValue, this.pairsOfConsonants));
			
		}
		
		for(char c: consonants)
		{
			char[] newValue = new char[password.length+1];
			System.arraycopy(password, 0, newValue, 0, password.length);
			System.arraycopy(new char[] {c}, 0, newValue,password.length,1);
			
			returnList.add(new PasswordState(this.maxSize, this.symbilcChars, newValue, this.pairsOfConsonants));
			
		}
		for(char c: symbols)
		{
			char[] newValue = new char[password.length+1];
			System.arraycopy(password, 0, newValue, 0, password.length);
			System.arraycopy(new char[] {c}, 0, newValue,password.length,1);
			
			returnList.add(new PasswordState(this.maxSize, this.symbilcChars, newValue, this.pairsOfConsonants));
			
		}
		
		
		return returnList;
		
	}

	@Override
	public boolean isSelect() {
		
		if(maxSize != password.length)
			return false;
		else
		{
			if(!condition1())return false;
			if(!condition2())return false;
			if(!condition3())return false;
			if(!condition4())return false;
			return true;
		}
		
		
	}
	
	
	public boolean condition1()
	{
		
		for(int i =1; i < password.length; i++)
		{
			if(password[i]==' ')return false;
			if(vowelsString.indexOf(password[i])>=0 &&vowelsString.indexOf(password[i-1])>=0 )
			{
				char secondVowel = vowelsString.charAt(vowelsString.indexOf(password[i]));
				char firstVowel = vowelsString.charAt(vowelsString.indexOf(password[i-1]));
				
				if(firstVowel == secondVowel)
				{
					return false;
				}
				
			}
		}
		return true;
	}
	
	public boolean condition2()
	{
		
		if(checkThreeConsecutive(vowelsString) == -1) return false;
		if(checkThreeConsecutive(consonantsString) == -1) return false;
		return true;
		
	
	}



	public boolean condition3()
	{
		if(pairsOfConsonants.isEmpty()) return true;
		
		for( int i = 1; i < password.length; i++)
		{
			String tempPair = String.valueOf( new char[]{password[i-1], password[i]});
			
			
			for(String neededPair: pairsOfConsonants)
			{
				if(tempPair.equals(neededPair)) return true;
			}

		}
		
		return false;
	}
	
	
	
	public boolean condition4()
	{
		for (int i = 0; i<password.length-symbilcChars-1; i++ )
		{
			if(symbolsString.indexOf(password[i])>=0)
			{
				return false;
			}
		}
		
		for(int i= password.length-symbilcChars-1; i< password.length; i++)
		{
			if(symbolsString.indexOf(password[i])<0)
			{
				return false;
			}
		}
		
		
		return true;
	}
	


	private int checkThreeConsecutive(String comparator) {
		for(int i =2; i < password.length; i++)
		{
			if(comparator.indexOf(password[i])>=0 &&comparator.indexOf(password[i-1])>=0 &&comparator.indexOf(password[i-2])>=0 )
			{
				return -1;
			}
		}
		return 1;
	}
	
	
	
	@Override
	public  String toString()
	{
		return String.valueOf(password);
	}



	public int getSize() {
		return password.length;
	}
	
	
	
	
	
	
	
	
}
