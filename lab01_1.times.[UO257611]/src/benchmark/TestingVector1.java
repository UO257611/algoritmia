package benchmark;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class TestingVector1 {

	public static void main(String[] args) {
		long[] fill = getArrayFillIn();
		
		long[] sum = getArraySum();
		
		long[] max = getArrayMax();
		
		writeInFile(fill, sum, max);
		

	}
	private static void writeInFile(long[] fill, long[] sum, long[] max) {
		try
		{
		
			BufferedWriter writer = new BufferedWriter(new FileWriter("Times_Vector"));
			writer.write("Vector 1\n");
			writer.write("Fill in data\n");
			for( long x : fill)
			{
				writer.write(x+"\n");
			}
			writer.write("Sum data\n");
			for( long x : sum)
			{
				writer.write(x+"\n");
			}
			writer.write("Fill in data\n");
			for( long x : max)
			{
				writer.write(x+"\n");
			}
			writer.close();
		} catch(Exception e)
		{
			
		}
		
 		
	}
	private static long[] getArrayMax() {
		
		long[] times = new long[17];
		int counter = 0;
		for (int i = 10 ; i<1291401627; i*=3)
		{
			int[] x = new int[i];
			Vector1.fillIn(x);
			long start = System.currentTimeMillis();
			int[] m = new int[2];
			Vector1.maximum(x, m);
			long end = System.currentTimeMillis();
			times[counter] = end - start;
			counter++;
			
		}
		
		return times;
	}
		
	
	public static long[] getArrayFillIn()
	{
		long[] times = new long[17];
		int counter = 0;
		for (int i = 10 ; i<1291401627; i*=3)
		{
			int[] x = new int[i];
			long start = System.currentTimeMillis();
			Vector1.fillIn(x);
			long end = System.currentTimeMillis();
			times[counter] = end - start;
			counter++;
			
		}
		
		return times;
	}
	
	
	public static long[] getArraySum()
	{
		long[] times = new long[17];
		int counter = 0;
		for (int i = 10 ; i<1291401627; i*=3)
		{
			int[] x = new int[i];
			Vector1.fillIn(x);
			long start = System.currentTimeMillis();
			int z =Vector1.sum(x);
			long end = System.currentTimeMillis();
			times[counter] = end - start;

			counter++;
			
		}
		
		return times;
	}

}
