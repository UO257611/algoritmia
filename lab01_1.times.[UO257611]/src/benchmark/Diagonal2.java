package benchmark;

import java.io.BufferedWriter;
import java.io.FileWriter;

/**
 * This program serves to measure times automatically increasing 
 * the size of the problem (n) and also using a time scale determined
 * by nTimes, which is taken from the argument arg[1]
 * It also gets as an execution argument (arg[0]) the operation on which 
 * we will focus the measurement (options 0,1,2 respectively)
 */
public class Diagonal2 {
	static int [][]a;

	@SuppressWarnings("unused")
	public static void main(String arg []) {
		/*int nTimes = Integer.parseInt(arg[1]); //nTimes
		int option = Integer.parseInt(arg[0]); //selected option
		long t1,t2;
		for ( int n = 3; n<=768 ; n *=2  )
		{
			int[][] a = new int[n][n];
		//n is incremented * 2 in each iteration
		   if (option==0){ //fill in the matrix
			   Diagonal1.fillIn(a);
		   } //if
		   else if (option==1) { //sum of the diagonal (one way)
			   Diagonal1.sum1Diagonal(a);
		   } //else if
		   else if (option==2) { //sum of the diagonal (another way)
			   Diagonal1.sum2Diagonal(a);
		   } //else if
		   else System.out.println("INCORRECT OPTION"); 
		}*/
		
		System.out.println("Start");
		long[] fill = getMatrixFillIn();
		System.out.println("Fill in done");
		long[] sum1 = getMatrixSum1Diagonal();
		System.out.println("Sum 1 done");
		long[] sum2 = getMatrixSum2Diagonal();

		System.out.println("Printing in file");
		writeInFile(fill, sum1, sum2);
	} //main
	
	
	public static long[] getMatrixFillIn()
	{
		long[] time = new long[14];
		int counter = 0;
		for ( int n = 3; n<49152 ; n*=2)
		{
			
			int[][] a = new int[n][n];
			long start = System.currentTimeMillis();
			for(int i= 0; i<150; i++)
				Diagonal1.fillIn(a);
			long end = System.currentTimeMillis();
			
			time[counter] = (end-start)/150;
			counter++;
			
			
		}
		return time;
		
	
	}
	
	
	public static long[] getMatrixSum1Diagonal()
	{
		long[] time = new long[14];
		int counter = 0;
		for ( int n = 3; n<49152; n*=2)
		{
			int[][] a = new int[n][n];
			Diagonal1.fillIn(a);
			int x;
			long start = System.currentTimeMillis();
			for(int i= 0; i<150; i++)
				x = Diagonal1.sum1Diagonal(a);
			
			long end = System.currentTimeMillis();
			time[counter] = (end-start)/150;
			counter++;
			
			
		}
		return time;
	}
	public static long[] getMatrixSum2Diagonal()
	{
		long[] time = new long[14];
		int counter = 0;
		int  x ;
		for ( int n = 3; n<49152; n*=2)
		{
			int[][] a = new int[n][n];
			Diagonal1.fillIn(a);
			
			long start = System.currentTimeMillis();
			for(int i= 0; i<150; i++)
				x = Diagonal1.sum2Diagonal(a);
			long end = System.currentTimeMillis();
			
			time[counter] = (end-start)/150;
			counter++;
			
			
		}
		return time;
	}
	
	
	private static void writeInFile(long[] fill, long[] sum1, long[] sum2) {
		try
		{
		
			BufferedWriter writer = new BufferedWriter(new FileWriter("Times_Diagonal"));
			writer.write("Diagonal\n");
			writer.write("Fill in data\n");
			for( long x : fill)
			{
				writer.write(x+"\n");
			}
			writer.write("Sum 1 data\n");
			for( long x : sum1)
			{
				writer.write(x+"\n");
			}
			writer.write("Sum 2 data\n");
			for( long x : sum2)
			{
				writer.write(x+"\n");
			}
			writer.close();
		} catch(Exception e)
		{
			
		}
		
 		
	}

} //class
